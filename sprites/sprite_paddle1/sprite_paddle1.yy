{
    "id": "00b99538-a52d-473e-8ee6-435358e06423",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_paddle1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b36854b-1f70-44da-ae97-4bf80ff05147",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00b99538-a52d-473e-8ee6-435358e06423",
            "compositeImage": {
                "id": "ee079545-71be-4ef8-8c1f-91ad9023bab3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b36854b-1f70-44da-ae97-4bf80ff05147",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "797a2f9d-c05f-4a46-a4bc-f6ed392626d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b36854b-1f70-44da-ae97-4bf80ff05147",
                    "LayerId": "3f3425b6-d19e-4c55-9b9f-3a6a76f959c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "3f3425b6-d19e-4c55-9b9f-3a6a76f959c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00b99538-a52d-473e-8ee6-435358e06423",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}