{
    "id": "dedd6238-eac5-49f6-a09c-ab1ef1dd3bbc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_goal",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 137,
    "bbox_left": -1,
    "bbox_right": 361,
    "bbox_top": 61,
    "bboxmode": 2,
    "colkind": 2,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4005314b-07ac-406d-8174-0a685b982727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dedd6238-eac5-49f6-a09c-ab1ef1dd3bbc",
            "compositeImage": {
                "id": "d6aa8dc8-0d85-4fc5-bc8b-65395804b09d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4005314b-07ac-406d-8174-0a685b982727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7eb14684-b42b-4b76-98fd-8d325219e328",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4005314b-07ac-406d-8174-0a685b982727",
                    "LayerId": "4478dc70-fcbd-4697-9217-1b76454c449f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 90,
    "layers": [
        {
            "id": "4478dc70-fcbd-4697-9217-1b76454c449f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dedd6238-eac5-49f6-a09c-ab1ef1dd3bbc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 360,
    "xorig": 180,
    "yorig": 89
}