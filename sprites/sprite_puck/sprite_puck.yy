{
    "id": "6af36725-2229-4ada-bef4-f5f79692b4d0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_puck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "914ddc04-d356-42a3-8d03-2e90bc7ee7e0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6af36725-2229-4ada-bef4-f5f79692b4d0",
            "compositeImage": {
                "id": "1a72896e-185e-47e4-ad7d-49cd0b50b683",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "914ddc04-d356-42a3-8d03-2e90bc7ee7e0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d738605f-4c2c-46cc-855d-e4fb06ef2b07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "914ddc04-d356-42a3-8d03-2e90bc7ee7e0",
                    "LayerId": "16ae7732-5931-445d-9779-d6c0e451632b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "16ae7732-5931-445d-9779-d6c0e451632b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6af36725-2229-4ada-bef4-f5f79692b4d0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}