{
    "id": "c9c639c8-c23f-4617-9966-55dc5dab07b3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_paddle2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0965edc2-b5b6-4f22-a9fa-aff64844ba94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9c639c8-c23f-4617-9966-55dc5dab07b3",
            "compositeImage": {
                "id": "2ad77abb-d6a6-45f6-a51f-f0ad56b0a650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0965edc2-b5b6-4f22-a9fa-aff64844ba94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a3b8c07a-bee4-4931-9446-c563483736a2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0965edc2-b5b6-4f22-a9fa-aff64844ba94",
                    "LayerId": "acfa6f86-33c0-4dda-b6c3-e928fbcd15ab"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "acfa6f86-33c0-4dda-b6c3-e928fbcd15ab",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9c639c8-c23f-4617-9966-55dc5dab07b3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 8
}