{
    "id": "849c5898-cee1-4a43-a1f9-3ccc59067302",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_paddle2",
    "eventList": [
        {
            "id": "a4a003ab-9125-4638-a4ad-fb372ef9064d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "849c5898-cee1-4a43-a1f9-3ccc59067302"
        },
        {
            "id": "7d707c4c-e1e7-4a65-9e68-a1f8148efe3c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "849c5898-cee1-4a43-a1f9-3ccc59067302"
        },
        {
            "id": "4bce2285-14cf-46f0-b651-4645b2b7bede",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "849c5898-cee1-4a43-a1f9-3ccc59067302"
        },
        {
            "id": "440b41fa-1be0-45fd-9f93-3265b8d2ea8a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "849c5898-cee1-4a43-a1f9-3ccc59067302"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9c639c8-c23f-4617-9966-55dc5dab07b3",
    "visible": true
}