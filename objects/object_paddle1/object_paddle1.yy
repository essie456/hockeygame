{
    "id": "4ebd36d7-e112-4215-90e1-7b3bf8425dff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_paddle1",
    "eventList": [
        {
            "id": "b41f5405-72d2-4a9f-8c12-93ef456018e4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4ebd36d7-e112-4215-90e1-7b3bf8425dff"
        },
        {
            "id": "e51e349d-a669-4dbc-9dd6-efc2d02a8500",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "4ebd36d7-e112-4215-90e1-7b3bf8425dff"
        },
        {
            "id": "af2fc76d-170c-45a1-a371-5e0e3eefa2a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "4ebd36d7-e112-4215-90e1-7b3bf8425dff"
        },
        {
            "id": "f82a9435-77f0-4ecc-8246-bbe8683f048e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "4ebd36d7-e112-4215-90e1-7b3bf8425dff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00b99538-a52d-473e-8ee6-435358e06423",
    "visible": true
}