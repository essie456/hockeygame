{
    "id": "5df10b47-2025-4297-96b4-39f5c9c5ae27",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_goal",
    "eventList": [
        {
            "id": "9aee3349-df96-4fe1-b02a-945191a1683d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d84e3a19-dfd2-4894-af13-28dac58600a8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5df10b47-2025-4297-96b4-39f5c9c5ae27"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dedd6238-eac5-49f6-a09c-ab1ef1dd3bbc",
    "visible": true
}