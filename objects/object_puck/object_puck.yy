{
    "id": "d84e3a19-dfd2-4894-af13-28dac58600a8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_puck",
    "eventList": [
        {
            "id": "15ee9b1e-5401-4239-8ea9-9f0f129169fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        },
        {
            "id": "3defbdce-e006-4a04-a74d-f37ee228e055",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        },
        {
            "id": "ac9aa0db-a093-4fcf-9bf6-6f884f4e403b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        },
        {
            "id": "18d8d789-0593-4c4a-a27f-fedb7be96459",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4ebd36d7-e112-4215-90e1-7b3bf8425dff",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        },
        {
            "id": "29caf0ac-d60a-4040-8e88-738f883fdef2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "849c5898-cee1-4a43-a1f9-3ccc59067302",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        },
        {
            "id": "f08ebe59-e8e1-47d0-8e61-6979b8fe7f47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 7,
            "m_owner": "d84e3a19-dfd2-4894-af13-28dac58600a8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6af36725-2229-4ada-bef4-f5f79692b4d0",
    "visible": true
}